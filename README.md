# Matomo Analytics helm chart
This helm chart provides you with a ready to use [Matomo Analytics](https://matomo.org) stack ready to deploy in your kubernetes cluster.
It provides:
 - Matomo
 - MariaDB

You can enable or disable every matomo dependency like mariadb or redis and also provide your own with your own configuration.
You are also able to re-configure most of all settings from the provided dependencies and defaults.

## Quick start

At first check all variables that need to be set, especially the credentials and secrets within the [values.yaml](values.yaml).
Do only use self-generated secrets and credentials for production environments.
```
helm upgrade --install -n matomo --create-namespace --set mariadb.auth.password=some-secret-db-pass,mariadb.auth.rootPassword=some-secret-admin-db-pass,mariadb.auth.database=some-custom-db-name matomo ./
```

To find out more configuration possibilities also check the [values.yaml](values.yaml).

## Example values.yaml with tls via cert-manager
```
secretKey: "to-generate-see-values-yaml-docs"
utilsSecret: "to-generate-see-values-yaml-docs"
ingress:
  host: matomo.somedomain.tld
  tls:
    enabled: true
    annotations:
      cert-manager.io/cluster-issuer: "letsencrypt-staging"
env:
mariadb:
  enabled: true
  architecture: "standalone"
  auth:
    rootPassword: "some-root-pw" # MUST be replaced with some secret password! Don't use this in production
    database: "matomo"
    username: "matomo"
    password: "some-pw" # MUST be replaced with some secret password! Don't use this in production
  persistence:
    enabled: true
    storageClass: ""
    size: 10Gi
```

## Contribute
Feel free to contribute and create pull requests. We will review and merge them.

### Credits
This is open source software by [encircle360](https://encircle360.com). Use on your own risk and for personal use. If you need support or consultancy just contact us.

